<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<nav class="navbar">
    <div class="container">
        <div class="navbar-header">
            <div class="navbar-brand">
                <a href="<?php echo home_url(); ?>">Kaisas konst</a>
            </div>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
            </button>
        </div>
        <div class="visible-lg visible-md">
	        <?php wp_nav_menu(
		        array (
                    'theme_location' => 'secondary',
			        'menu_class' => 'nav navbar-nav pull-right',
			        'depth' => 2,
			        'container' => '',
                    )); ?>
        </div>

        <a class="cart-item pull-right" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'Gå till kassan' ); ?>">
        <div class="cart-contents">
        <i class="fa fa-shopping-bag"></i>
        <span class="cart-count"></span>
        </div>
        </a>

        <div class="collapse navbar-collapse" id="myNavbar">
	        <?php wp_nav_menu(
		        array (
			        'theme_location' => 'primary',
			        'menu_class' => 'nav navbar-nav',
			        'depth' => 2,
			        'container' => '',
                    )); ?>
        </div>
    </div>
</nav>

<div class="container">
<div class="wrapper">