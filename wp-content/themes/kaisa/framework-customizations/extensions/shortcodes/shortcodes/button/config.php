<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'kaisas-button', 'fw' ),
	'description' => __( 'Add a Button', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' ),
	'popup_size' => 'small'
);
