</div><!-- /.row -->
    </div><!-- /.container -->
        </div>
    <footer>
    <div class="kaisa-footer">
        <div class="container">
            <div id="footer-sidebar" class="secondary row">
                <div id="footer-sidebar1" class="col-md-3 col-sm-6">
		            <?php
		            if(is_active_sidebar('footer-sidebar-1')){
			            dynamic_sidebar('footer-sidebar-1');
		            } ?>
                </div>
                <div id="footer-sidebar2" class="col-md-3 col-sm-6">
		            <?php
		            if(is_active_sidebar('footer-sidebar-2')){
			            dynamic_sidebar('footer-sidebar-2');
		            } ?>
                </div>
                <div id="footer-sidebar3" class="col-md-3 col-sm-6">
		            <?php
		                if(is_active_sidebar('footer-sidebar-3')){
			                dynamic_sidebar('footer-sidebar-3');
		                } ?>
                </div>
                <div id="footer-sidebar4" class="col-md-3 col-sm-6">
		            <?php
		                if(is_active_sidebar('footer-sidebar-4')){
			                dynamic_sidebar('footer-sidebar-4');
		                } ?>
                </div>
                <p class="text-center copyright-text">COPYRIGHT © <?php echo date("Y"); ?>, KAISAS KONST</p>
            </div>
        </div>
    </div>
    </footer>
</div><!--wrapper-->
<?php wp_footer(); ?>
</body>
</html>
