<?php

function kaisa_enqueue() {
    wp_register_style('kaisa_bootstrap' , get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.css');
    wp_register_style('kaisa_main' , get_template_directory_uri() . '/assets/css/main.css');
    wp_register_style('kaisa_sass' , get_template_directory_uri() . '/assets/dist/css/app.css');
    wp_register_style('kaisa_fonts' , 'https://fonts.googleapis.com/css?family=Oranienbaum|Roboto:300');



	wp_enqueue_style('kaisa_bootstrap');
	wp_enqueue_style('kaisa_main');
	wp_enqueue_style('kaisa_sass');
	wp_enqueue_style('kaisa_custom-woo');
	wp_enqueue_style('kaisa_fonts');
	wp_enqueue_style('kaisa_font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

	wp_register_script('kaisa_bootstrap' , get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.js', array(), FALSE, TRUE);

	wp_enqueue_script('kaisa-js', get_stylesheet_directory_uri() . '/assets/js/script.js', array('jquery'), false, TRUE);
	wp_enqueue_script('kaisa_bootstrap');

}
