<?php
//varukorg Test - ej klar

function my_header_add_to_cart_fragment( $fragments ) {

	ob_start();
	$count = WC()->cart->cart_contents_count;
	?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'Gå till kassan' ); ?>"><?php
	if ( $count > 0 ) {
		?>
		<span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
		<?php
	}
	?></a><?php

	$fragments['a.cart-contents'] = ob_get_clean();

	return $fragments;
}
