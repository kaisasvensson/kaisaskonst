<?php
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
}

function new_loop_shop_per_page( $cols ) {
	$cols = 30;
	return $cols;
}

function woo_hide_page_title() {
	return false;
}

if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3;
	}
}

function custom_remove_woo_checkout_fields( $fields ) {
	unset($fields['billing']['billing_company']);
	unset($fields['billing']['billing_address_2']);
	unset($fields['shipping']['shipping_company']);
	unset($fields['shipping']['shipping_address_2']);
	unset($fields['order']['order_comments']);

	return $fields;
}

function woo_remove_product_tabs( $tabs ) {

	unset( $tabs['description'] );
	unset( $tabs['reviews'] );
	unset( $tabs['additional_information'] );

	return $tabs;
}

function error_message_swe( $error ) {
	return str_replace('Billing','',$error);
}

function woocommerce_remove_logout_confirmation() {
	global $wp;

	if ( isset( $wp->query_vars['customer-logout'] ) ) {
		wp_redirect( str_replace( '&amp;', '&', wp_logout_url( wc_get_page_permalink( 'home' ) ) ) );
		exit;
	}
}
