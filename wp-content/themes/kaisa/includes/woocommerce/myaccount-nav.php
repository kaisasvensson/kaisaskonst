<?php
/* Add custom menu item and endpoint to WooCommerce My-Account page */

function my_custom_endpoints() {
	add_rewrite_endpoint( 'refunds-returns', EP_ROOT | EP_PAGES );
}

add_action( 'init', 'my_custom_endpoints' );

function my_custom_query_vars( $vars ) {
	$vars[] = 'refunds-returns';

	return $vars;
}

add_filter( 'query_vars', 'my_custom_query_vars', 0 );

function my_custom_flush_rewrite_rules() {
	flush_rewrite_rules();
}

add_action( 'after_switch_theme', 'my_custom_flush_rewrite_rules' );

function my_custom_my_account_menu_items( $items ) {
	$items = array(
		'orders'            => __( 'Mina beställningar', 'woocommerce' ),
		'edit-account'      => __( 'Min profil', 'woocommerce' ),
		'customer-logout'   => __( 'Logga ut', 'woocommerce' ),
	);

	return $items;
}

add_filter( 'woocommerce_account_menu_items', 'my_custom_my_account_menu_items' );

function my_custom_endpoint_content() {
	include 'woocommerce/myaccount/refunds-returns.php';
}

