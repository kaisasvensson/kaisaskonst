<?php

function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();

	?>
	<span class="cart-count"><?php echo $woocommerce->cart->cart_contents_count; ?></span>
	<?php

	$fragments['span.cart-count'] = ob_get_clean();

	return $fragments;

}