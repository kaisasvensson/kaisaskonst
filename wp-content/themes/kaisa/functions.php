<?php

/*
 * ------------ SET UP --------------
 * */
add_theme_support('menus');
add_theme_support( 'custom-header' );

/*
 * ----------- INCLUDES --------------
 * */

include(get_template_directory() . '/includes/front/enqueue.php');
include(get_template_directory() . '/includes/front/menu-setup.php');
include(get_template_directory() . '/includes/widgets.php');
include(get_template_directory() . '/includes/woocommerce/woocommerce-setup.php');
include(get_template_directory() . '/includes/woocommerce/myaccount-nav.php');
include(get_template_directory() . '/includes/woocommerce/cart-count.php');
include(get_template_directory() . '/includes/front/cart-count.php');


/*
 * ---- ACTION AND FILTER HOOKS -----
 * */

add_action('wp_enqueue_scripts', 'kaisa_enqueue');
add_action('after_setup_theme', 'kaisa_setup_theme');
add_action('widgets_init', 'kaisa_widgets');

add_action( 'after_setup_theme', 'woocommerce_support' );
add_action( 'woocommerce_account_refunds-returns_endpoint', 'my_custom_endpoint_content' );

//antal varor i kundkorg
add_filter( 'woocommerce_add_to_cart_fragments', 'my_header_add_to_cart_fragment' );

//Visa max 30 tavlor per sida
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 30 );

//Tar bort page title
add_filter( 'woocommerce_show_page_title' , 'woo_hide_page_title' );

//3 columner istället för 4 (produkter)
add_filter('loop_shop_columns', 'loop_columns');

//tar bort de fält i checkout som jag inte tycker är viktiga
add_filter( 'woocommerce_checkout_fields' , 'custom_remove_woo_checkout_fields' );

//Tar  bort meddelande om att varan har lagts i kundkorg.
add_filter( 'wc_add_to_cart_message_html', '__return_null' );

//tar bort "sortera" dropdown
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

//ta bort taggar på produktsida
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

add_filter( 'woocommerce_add_error', 'error_message_swe' );

add_action( 'template_redirect', 'woocommerce_remove_logout_confirmation' );



add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

