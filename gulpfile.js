const gulp = require('gulp');
const sass = require('gulp-sass');
const buffer = require('vinyl-buffer');
const source = require('vinyl-source-stream');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const babelify = require('babelify');
const browserify = require('browserify');
const watchify = require('watchify');
const rename = require('gulp-rename');
const postcss = require('gulp-postcss');
// const imagemin = require('gulp-imagemin');
const plumber = require('gulp-plumber');
const sequence = require('run-sequence');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const del = require('del');
// const livereload = require('gulp-livereload');
// const inject = require('gulp-inject');


/**
 * Settings
 */
const ES6_SYNTAX_IS_VERY_COOL = true;
const BASE = 'wp-content/themes/kaisa/assets/';


const PATHS = {
    styles: {
        entry: BASE + 'src/sass/app.scss',
        src: [
            BASE + 'src/sass/**/*.scss'
        ],

        dest: BASE + 'dist/css/'
    },
    scripts: {
        entry: BASE + 'src/js/app.js',
        vendor: BASE + 'src/js/vendor/**/*.js',

        // Need specific ordering?
        // vendor: [
        //     BASE + 'src/js/vendor/my-first-file.js',
        //     BASE + 'src/js/vendor/my-second-file.js',
        //     BASE + 'src/js/vendor/**/*.js'
        // ],

        src: [
            BASE + 'src/js/**/*.js',
        ],
        dest: BASE + 'dist/js/'
    },
};

/**
 * Compile the SCSS with gulp-sass, autoprefixer and cssnano
 *
 * Plumber let's the task continue on error without crashing
 * Cssnano and autoprefixer are postcss plugins which minify and prefix your css
 */
gulp.task('styles', () => {
    return gulp.src(PATHS.styles.entry)
        .pipe(plumber())
        .pipe(sourcemaps.init('.'))
        .pipe(sass())
        .on('error', function (err) {
            console.error('\n' + err.toString() + '\n');
            this.emit("end");
        })
        .pipe(postcss([
            autoprefixer({
                browsers: ['last 3 versions', 'iOS > 7']
            }),
            cssnano()
        ]))
        .pipe(rename({
            basename: 'app'
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(PATHS.styles.dest));
});


/**
 * Compile or concatenate javascript depending on how ES6_SYNTAX_IS_VERY_COOL is set.
 * If true, utilize import or require()-statements. If false, simply concatenate all javascript files.
 */
gulp.task('scripts', () => {

    if (ES6_SYNTAX_IS_VERY_COOL) {

        // Return the Browserify task
        return sequence('browserify');

    }

    return gulp.src([PATHS.scripts.src, '!' + PATHS.scripts.vendor])
        .pipe(plumber())
        .pipe(sourcemaps.init('.'))
        .pipe(uglify())
        .pipe(concat('app.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(PATHS.scripts.dest))

});


/**
 * Browsers do not support require()-syntax, and Browserify solves this.
 * Start from the entry point and recursively compile imported files.
 */
gulp.task('browserify', () => {
    return browserify(PATHS.scripts.entry)
        .transform(babelify, {presets: ['es2015']})
        .bundle()
        .on('error', e => {
            console.log(e.toString())
        })
        .pipe(plumber())
        .pipe(source('app.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init('.'))
        .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(PATHS.scripts.dest));
});


/**
 * Run all the build tasks
 */
gulp.task('watch', ['build'], () => {
    gulp.watch(PATHS.styles.src, ['styles']);
    gulp.watch([
        PATHS.scripts.src,
        '!' + PATHS.scripts.vendor
    ], ['scripts']);

});

/**
 * Run all the build tasks
 */
gulp.task('build', () => {
    sequence(['styles','scripts']);
});

gulp.task('default', ['watch']);