<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kaisaskonst');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'YP#,u?Un5H%BUDN%I6m^G34@L!*fJ9G)J] zCBb8?;$g^3(-HRKh=`+HO>2MfGC&');
define('SECURE_AUTH_KEY',  '~(70>]k RTlA9`x{5=mqmyCe^Gcm]& mn)`)6T]`nEm4g,M8@a<8Lz7}.S0!rim=');
define('LOGGED_IN_KEY',    '4kuEh|uOq81058f1vY0;:T{{7!ay^.n(tW~R2</A&pL?*(H<wp7- v`FA)_c5Qgx');
define('NONCE_KEY',        'DT 0hQ;m.(;CYbuEOCT?weo]|>S2:1~m?tcQP+{Q?`)[6HS&m1H2^Y)[G~zJ!~[d');
define('AUTH_SALT',        'Z(w%1V@yO 0uWt9>5}tY,ZW7$is7b|rho^gQc*]9;)K.[l^R?u.HBV+?1%<^#VAT');
define('SECURE_AUTH_SALT', '$|[HS1{;)Tz@l!|3`[$e.yad2DJ+86XX`JJEtP.[e@gz}E%l^}`0AR:ur,qft#s{');
define('LOGGED_IN_SALT',   'x>1 OHQJCzC)Pc~OK!ViCBcPsP5y3zM{m?@FAB}1g7?;o_]n`&dPgL_/mA?(0c+a');
define('NONCE_SALT',       '=9~6!u?wBY58iG]yW ;@rlbe|e7~.>6]QKqZ8fug,tKzr;s`|L*6mq]%n$,5AC`t');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define( 'WP_MEMORY_LIMIT', '256M' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
